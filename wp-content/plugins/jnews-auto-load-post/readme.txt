Change Log :

== 6.0.1 ==
- [IMPROVEMENT] Only load the autoload script file when needed
- [IMPROVEMENT] Add lazyload image for image ad on the autoload separator

== 6.0.0 ==
- [IMPROVEMENT] Compatible with JNews v6.0.0

== 5.0.0 ==
- [IMPROVEMENT] Compatible with JNews v5.0.0

== 4.0.0 ==
- [IMPROVEMENT] Compatible with JNews v4.0.0

== 3.0.1 ==
- [IMPROVEMENT] Remove all white space on Google ad ID

== 3.0.0 ==
- [IMPROVEMENT] Compatible with JNews v3.0.0

== 2.0.1 ==
- [IMPROVEMENT] Add 4 new single post layout

== 2.0.0 ==
- [IMPROVEMENT] Compatible with JNews v2.0.0

== 1.1.0 ==
- [IMPROVEMENT] Add option to autoload filtered by category, and tag

== 1.0.3 ==
- [BUG] Fix url on response contain autoload url

== 1.0.2 ==
- [BUG] Fix window title contain HTML tag

== 1.0.1 ==
- [BUG] Fix issue when Autoload used along with JNews - Split
